 var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app     = express();

var franchise=[];
var movie=[];
var tv=[];
var celeb=[];
var prod=[];
var $keyword;

app.use(express.static('files'));
app.use(express.static('pages'));
app.listen(process.env.PORT || '5000')
console.log('Magic happens on port 5000');

app.get('/', function(req, res){
	res.sendFile(__dirname+'/index.html');
});
app.get('/send_url',function(req, res){
	getdata(req.query.keyword,res);
	//getdir('http://www.rottentomatoes.com/m/love_actually/','Movies',res);
});
app.get('/load_data',function(req,res){
	var url='http://www.rottentomatoes.com';
	var new_url=url+req.query.dir;
	getdir(new_url,req.query.cat,res);
});
function getdir(url,category,res)
{
	console.log(url);
	request(url, function(error, response, html){
		if(!error){
			 var $ = cheerio.load(html);
			 if(category==='Movies'){
			 	var obj=[];
				var $all=$('#movie-image-section');
				var detail=$('#movieSynopsis').html();
				var $img=$all.find('img').attr('src');
				$img="url('"+$img+"'";
				console.log($img);
				obj=[{"image": $img,"detail": detail}];
				console.log(obj);
			 }
			}
			else
			{
				return console.log(error);
			}
			console.log();
			res.send(obj);
			obj=[];
  	});
}
function getdata(keyword,res)
{
	// Let's scrape Anchorman 2
	var url='http://www.rottentomatoes.com/search/?search='+keyword;
	console.log(url);
	request(url, function(error, response, html){
		if(!error){
			 var $ = cheerio.load(html);
			 var $all=$('#results_all_tab');
			 
			 $all.find('ul').each(function(i,e)
			 {
			 		if($(this).attr('id')==='critic_results_ul')
			 		{
			 		$franchise=$(this);
					 $franchise.find('li').each(function(index,element){
					 	$img=$(this).children().first().find('img').attr('src');
					 	$title=$(this).children().first().next().find('a').html();
					 	$url=$(this).children().first().next().find('a').attr('href');
					 	$cat=$franchise.attr('id');
					 	franchise.push(
					 		{
					 			"image":$img, 
					 			"title":$title, 
					 			"url":$url, 
					 			"category":$cat 
					 		});
					 });
					 //console.log(franchise);
					}
					if($(this).attr('id')==='movie_results_ul')
			 		{
			 		$movie=$(this);
					 $movie.find('li').each(function(index,element){
					 	$img=$(this).children().first().next().find('img').attr('src');
					 	$title=$(this).children().first().next().next().children().first().find('a').html();
					 	$url=$(this).children().first().next().next().children().first().find('a').attr('href');
					 	$cat=$movie.attr('id');
					 	movie.push(
					 		{
					 			"image":$img, 
					 			"title":$title, 
					 			"url":$url, 
					 			"category":$cat 
					 		});
					 });
					 //console.log(movie);
					}
					if($(this).attr('id')==='tv_results_ul')
			 		{
			 		$tv=$(this);
					 $tv.find('li').each(function(index,element){
					 	$img=$(this).children().first().next().find('img').attr('src');
					 	$title=$(this).children().first().next().next().children().first().find('a').html();
					 	$url=$(this).children().first().next().next().children().first().find('a').attr('href');
					 	$cat=$tv.attr('id');
					 	tv.push(
					 		{
					 			"image":$img, 
					 			"title":$title, 
					 			"url":$url, 
					 			"category":$cat 
					 		});
					 });
					 // console.log(tv);
					}
					if($(this).attr('id')==='actor_results_ul')
			 		{
			 		$celeb=$(this);
					$celeb.find('li.media').each(function(index,element){
					 	$img=$(this).children().first().find('img').attr('src');
					 	$title=$(this).children().first().next().children().first().find('a').html();
					 	$url=$(this).children().first().next().children().first().find('a').attr('href');
					 	$cat=$celeb.attr('id');
					 	celeb.push(
					 		{
					 			"image":$img, 
					 			"title":$title, 
					 			"url":$url, 
					 			"category":$cat 
					 		});
					 });
					//console.log(celeb);
					}		
				});
				prod.push(franchise,movie,tv,celeb);
				
		}
		else
		{
			return console.log(error);
		}
		res.send(prod);
		franchise=[];
		movie=[];
		tv=[];
		celeb=[];
		prod=[];
  	});
}

